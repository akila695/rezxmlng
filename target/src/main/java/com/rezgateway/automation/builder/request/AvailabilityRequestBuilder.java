package com.rezgateway.automation.builder.request;

import java.io.FileWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.Attribute;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.rezgateway.automation.enu.SearchBy;
import com.rezgateway.automation.pojo.AvailabilityRequest;
import com.rezgateway.automation.pojo.Room;

public class AvailabilityRequestBuilder {

	private static Logger logger = null;

	public AvailabilityRequestBuilder() {
		logger = Logger.getLogger(AvailabilityRequestBuilder.class);
	}

	public Document getDocument(AvailabilityRequest reqobject) throws Exception {

		Document doc = new Document();
		Element availabilityRequestRoot = new Element("availabilityRequest");
		// SetRoot Element
		// Set availabilityRequest attributes
		try {
			logger.info("........availabilityRequest_Document Starting.........."+ reqobject.getScenarioID());
			availabilityRequestRoot.setAttribute(new Attribute("cancelpolicy", reqobject.getAvailabitlyCancerlatioPolicy()));
			availabilityRequestRoot.setAttribute(new Attribute("hotelfees", reqobject.getAvailabitlyHotelFee()));
			doc.setRootElement(availabilityRequestRoot);

			Element control = new Element("control");
			control.addContent(new Element("username").setText(reqobject.getUserName()));
			control.addContent(new Element("password").setText(reqobject.getPassword()));
			doc.getRootElement().addContent(control);

			// set CheckinCheckout /No of Rooms /no of Nights / Country / State
			availabilityRequestRoot.addContent(new Element("Checkin").setText(reqobject.getCheckin()));
			availabilityRequestRoot.addContent(new Element("checkOut").setText(reqobject.getCheckout()));
			availabilityRequestRoot.addContent(new Element("noOfRooms").setText(Integer.toString(reqobject.getRoomlist().size())));
			availabilityRequestRoot.addContent(new Element("noOfNights").setText(reqobject.getNoofNights()));
			availabilityRequestRoot.addContent(new Element("country").setText(reqobject.getCountry()));
			availabilityRequestRoot.addContent(new Element("state").setText(reqobject.getState()));

			
			
			// Search Type Configuration- Start
			if (SearchBy.HOTELCODE == reqobject.getSearchType()) {
				Element hotelcodes = new Element("hotelCodes");
				String[] hc = reqobject.getCode();
				for (int i = 0; i < hc.length; i++) {
					
					hotelcodes.addContent(new Element("hotelCode").setText(hc[i]));
				}
				doc.getRootElement().addContent(hotelcodes);
			} else if (SearchBy.IATACODE == reqobject.getSearchType()) {
				availabilityRequestRoot.addContent(new Element("IATAAirportCode").setText(reqobject.getCode()[0]));
				Element hotelcodes = new Element("hotelCodes");
				hotelcodes.addContent(new Element("hotelCode").setText("0"));
				doc.getRootElement().addContent(hotelcodes);
				
			} else if (SearchBy.CITYCODE == reqobject.getSearchType()) {
				availabilityRequestRoot.addContent(new Element("city").setText(reqobject.getCode()[0]));
				
				Element hotelcodes = new Element("hotelCodes");
				hotelcodes.addContent(new Element("hotelCode").setText("0"));
				doc.getRootElement().addContent(hotelcodes);
				
			} else if (SearchBy.HOTELGROUPCODE == reqobject.getSearchType()) {
				availabilityRequestRoot.addContent(new Element("hotelGroupCode").setText(reqobject.getCode()[0]));
				
				Element hotelcodes = new Element("hotelCodes");
				hotelcodes.addContent(new Element("hotelCode").setText("0"));
				doc.getRootElement().addContent(hotelcodes);
				
			} else {
				logger.error(".......ErrorSeeachByType  Selection of the " + reqobject.getScenarioID());
				availabilityRequestRoot.addContent(new Element("ErrorSeeachByType").setText(" ErrorSeeachByType  Selection "));
			}
			// if(hc.length==1){
			// Search Type Configuration End
			Element roomsInformation = new Element("roomsInformation");
			doc.getRootElement().addContent(roomsInformation);

			// Room Details Generating Start
			for (Room r : reqobject.getRoomlist()) {
				Element roomInfo = new Element("roomInfo");
				roomsInformation.addContent(roomInfo);
				roomInfo.addContent(new Element("roomTypeId").setText("0"));
				roomInfo.addContent(new Element("bedTypeId").setText("0"));
				roomInfo.addContent(new Element("adultsNum").setText(r.getAdultsCount()));
				roomInfo.addContent(new Element("childNum").setText(r.getChildCount()));

				// Child Ages Details Generating Start
				if (r.getChildCount() != null) {
					if (Integer.parseInt(r.getChildCount().trim()) != 0) {
						try {
							if ((Integer.parseInt(r.getChildCount().trim()) == r.getChildAges().length)) {
								if ((Integer.parseInt(r.getChildCount().trim()) > 0)) {
									Element childAges = new Element("childAges");
									roomInfo.addContent(childAges);
									for (String temp : r.getChildAges()) {
										childAges.addContent(new Element("childAge").setText(temp));
									}
									// Child Ages Details Generating End
								}
							}
						} catch (NumberFormatException e) {
							logger.error(".......Invalid number of children provided......."+ reqobject.getScenarioID());
							roomInfo.addContent(new Element("Error").setText("Invalid number of children provided"));
							throw new Exception("Invalid number of children provided ");
						}
					} else {
						
					}
				} else {
					logger.error(".......Child Count is Null......."+ reqobject.getScenarioID());
					roomInfo.addContent(new Element("Error").setText("Child Count is Null"));
					throw new Exception("Child Count is Null"+ reqobject.getScenarioID());
				}
				// Room Details Generating End
			}
			logger.info("........availabilityRequest_Document Ending.........."+ reqobject.getScenarioID());
			/*
			 * }else if(hc.length==0){ hotelcodes.addContent(new Element("ERROR").setText("Hotel codes are Not entered or hotel Codes are Empty"));
			 * logger.error("........Hotel codes are Not entered or hotel Codes are Empty.........."); }
			 */
		} catch (NumberFormatException e) {
			logger.error(".......Invalid number of children provided ......."+ reqobject.getScenarioID(), e);
			throw new NumberFormatException(".Invalid number of children provided."+ reqobject.getScenarioID());
		} catch (Exception e) {
			logger.fatal("Error While Building Document"+ reqobject.getScenarioID(), e);
		}
		return doc;
	}

	// For get String and XML Output
	public String buildRequest(String Path, AvailabilityRequest reqobject) {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getDocument(reqobject);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, new FileWriter(Path));
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Request :"+ reqobject.getScenarioID()+ es);
		}
		return res;
	}

	// For get String Output
	public String buildRequest(AvailabilityRequest reqobject) {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getDocument(reqobject);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Request :" + reqobject.getScenarioID()+ es);
		}
		return res;
	}
}
