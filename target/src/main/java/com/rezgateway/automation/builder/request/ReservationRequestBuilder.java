package com.rezgateway.automation.builder.request;

import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Random;

import org.apache.log4j.*;
import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.Attribute;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.rezgateway.automation.enu.ConfirmationType;
import com.rezgateway.automation.pojo.ReservationRequest;
import com.rezgateway.automation.pojo.Room;

public class ReservationRequestBuilder {

	private static Logger logger = null;

	public ReservationRequestBuilder() {

		logger = Logger.getLogger(this.getClass());
	}

	public Document getDocument(ReservationRequest resObject) {

		Document doc = new Document();
		Element reservationRequest = new Element("reservationRequest");

		try {
			logger.info("........_ReservationRequest_Document Starting.........."+resObject.getScenarioID());
			reservationRequest.setAttribute(new Attribute("returnCompeleteBookingDetails", resObject.getReturnCompeleteBookingDetails()));
			doc.setRootElement(reservationRequest);
			Element controls = new Element("control");
			controls.addContent(new Element("userName").setText(resObject.getUserName()));
			controls.addContent(new Element("passWord").setText(resObject.getPassword()));
			doc.getRootElement().addContent(controls);
			Element reservationDetails = new Element("reservationDetails");
			doc.getRootElement().addContent(reservationDetails);
			reservationDetails.setAttribute("timeStamp", resObject.getReservationDetailsTimeStamp());

			if (ConfirmationType.CON == resObject.getConfType()) {
				reservationDetails.addContent(new Element("confirmationType").setText("CON"));
			} else if (ConfirmationType.REQ == resObject.getConfType()) {
				reservationDetails.addContent(new Element("confirmationType").setText("REQ"));
			} else {
				logger.error("........ERROR_ConfType_Selection.........."+resObject.getScenarioID());
				reservationDetails.addContent(new Element("confirmationType").setText("ERROR_ConfType_Selection"));
			}
			reservationDetails.addContent(new Element("tourOperatorOrderNumber").setText("20113217T03:32:20"));
			reservationDetails.addContent(new Element("checkIn").setText(resObject.getCheckin()));
			reservationDetails.addContent(new Element("checkOut").setText(resObject.getCheckout()));
			reservationDetails.addContent(new Element("noOfRooms").setText(Integer.toString((resObject.getRezRoomList().size()))));
			reservationDetails.addContent(new Element("noOfNights").setText(resObject.getNoofNights()));
			reservationDetails.addContent(new Element("hotelCode").setText(resObject.getCode()[0]));
			reservationDetails.addContent(new Element("total").setAttribute("currency", resObject.getCurrency()).setText(resObject.getTotal()));
			reservationDetails.addContent(new Element("totalTax").setAttribute("currency", resObject.getCurrency()).setText(resObject.getTotalTax()));

			if ((resObject.getRezRoomList().size() > 0) && (resObject.getRezRoomList() != null)) {

				for (Room r : resObject.getRezRoomList()) {
					
					Element roomData = new Element("roomData");
					reservationDetails.addContent(roomData);

					roomData.addContent(new Element("roomNo").setText(r.getRoomNo()));
					roomData.addContent(new Element("roomCode").setText(r.getRoomCode()));
					roomData.addContent(new Element("roomTypeCode").setText(r.getRoomTypeID()));
					roomData.addContent(new Element("bedTypeCode").setText(r.getBedTypeID()));
					roomData.addContent(new Element("ratePlanCode").setText(r.getRatePlanCode()));
					roomData.addContent(new Element("noOfAdults").setText(r.getAdultsCount()));
					roomData.addContent(new Element("noOfChildren").setText(r.getChildCount()));

					// Note >>>>>> Last name and First name Counts Should be Equal to adults+Child Count
					int adultcount = Integer.parseInt((r.getAdultsCount()));
					int childcount = Integer.parseInt((r.getChildCount()));
					
					int paxCount = (adultcount + childcount);
				

					if (paxCount!= 0) {
						Element occupancy = new Element("occupancy");
				        for (int i = 0; i < adultcount; i++) {
							
							Element guest = new Element("guest");
							guest.addContent(new Element("title").setText("Mr"));
							guest.addContent(new Element("firstName").setText("adult"+new Random().nextInt(1000)));
							guest.addContent(new Element("lastName").setText("last"+new Random().nextInt(1000)));
							occupancy.addContent(guest);
						}
						
						for (int i = 0; i < childcount; i++) {
							Element guest = new Element("guest");
							guest.addContent(new Element("title").setText("Child."));
							guest.addContent(new Element("firstName").setText("child"+new Random().nextInt(1000)));
							guest.addContent(new Element("lastName").setText("last"+new Random().nextInt(1000)));
							guest.addContent(new Element("age").setText(r.getChildAges()[i]));
							occupancy.addContent(guest);
						

						}
						roomData.addContent(occupancy);
					} else {
						logger.error("........Error PaxCount entered......."+paxCount+"..."+resObject.getScenarioID());
						roomData.addContent(new Element("error")).setText("Error PaxCount entered");
						//doc.getRootElement().addContent(reservationDetails);
						throw new Exception(" name Count and PAX count is not matched ");
					}
				}
				//doc.getRootElement().addContent(reservationDetails);
			} else {
				logger.error("........No Rooms Availble for This hotel or No Rooom Data Provided.........."+resObject.getScenarioID());
				reservationDetails.addContent(new Element("error").setText("No Rooms Availble for This hotel or No Rooom Data Provided"));
				//doc.getRootElement().addContent(reservationDetails);
			}

			Element comment = new Element("comment");
			reservationDetails.addContent(comment);
			comment.addContent(new Element("customer").setText(resObject.getUserComment()));
			comment.addContent(new Element("hotel").setText(resObject.getHotelComment()));
			logger.info("........_ReservationRequest_Document Ending..........");

		} catch (Exception e) {
			logger.fatal("Error While Building Document :"+resObject.getScenarioID(), e);
		}

		return doc;
	}

	public String buildRequest(String Path, ReservationRequest resObj) throws Exception {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getDocument(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, new FileWriter(Path));
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document :"+resObj.getScenarioID(), es);
		}
		return res;
	}

	public String buildRequest(ReservationRequest resObj) {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getDocument(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document :"+resObj.getScenarioID(), es);
		}
		return res;

	}

}
