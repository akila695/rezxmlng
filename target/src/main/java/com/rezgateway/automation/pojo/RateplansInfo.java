package com.rezgateway.automation.pojo;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class RateplansInfo {
	
	private String RatePlanCode = "0";
	private String RatePlan = "N/A";
	private double AverageRate = 0.0;
	private double TotalRate = 0.0;
	private TreeMap<String,DailyRates> dailyRates = new TreeMap<String, DailyRates>();
	private Map<String, Tax> TaxInfor = new HashMap<String,Tax>();
	private Map<String, HotelFees> HotelFeeInfo = new HashMap<String,HotelFees>();
	
	/**
	 * @return the ratePlanCode
	 */
	public String getRatePlanCode() {
		return RatePlanCode;
	}
	/**
	 * @param ratePlanCode the ratePlanCode to set
	 */
	public void setRatePlanCode(String ratePlanCode) {
		RatePlanCode = ratePlanCode;
	}
	/**
	 * @return the ratePlan
	 */
	public String getRatePlan() {
		return RatePlan;
	}
	/**
	 * @param ratePlan the ratePlan to set
	 */
	public void setRatePlan(String ratePlan) {
		RatePlan = ratePlan;
	}
	/**
	 * @return the averageRate
	 */
	public double getAverageRate() {
		return AverageRate;
	}
	/**
	 * @param averageRate the averageRate to set
	 */
	public void setAverageRate(double averageRate) {
		AverageRate = averageRate;
	}
	/**
	 * @return the totalRate
	 */
	public double getTotalRate() {
		return TotalRate;
	}
	/**
	 * @param totalRate the totalRate to set
	 */
	public void setTotalRate(double totalRate) {
		TotalRate = totalRate;
	}
	/**
	 * @return the dailyRates
	 */
	public TreeMap<String, DailyRates> getDailyRates() {
		return dailyRates;
	}
	/**
	 * @param dailyRates the dailyRates to set
	 */
	public void setDailyRates(TreeMap<String, DailyRates> dailyRates) {
		this.dailyRates = dailyRates;
	}
	/**
	 * @return the taxInfor
	 */
	public Map<String, Tax> getTaxInfor() {
		return TaxInfor;
	}
	/**
	 * @param taxInfor the taxInfor to set
	 */
	public void setTaxInfor(Map<String, Tax> taxInfor) {
		TaxInfor = taxInfor;
	}
	/**
	 * @return the hotelFeeInfo
	 */
	public Map<String, HotelFees> getHotelFeeInfo() {
		return HotelFeeInfo;
	}
	/**
	 * @param hotelFeeInfo the hotelFeeInfo to set
	 */
	public void setHotelFeeInfo(Map<String, HotelFees> hotelFeeInfo) {
		HotelFeeInfo = hotelFeeInfo;
	}

	
	
 	

}
