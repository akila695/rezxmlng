package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;

import com.rezgateway.automation.enu.ConfirmationType;
import com.rezgateway.automation.pojo.RateplansInfo;
//import com.rezgateway.automation.pojo.Hotel;
import com.rezgateway.automation.pojo.ReservationResponse;
import com.rezgateway.automation.pojo.Room;

public class ReservationResponseReader {

	private static Logger logger = null;

	public ReservationResponseReader() {
		logger = Logger.getLogger(ReservationResponseReader.class);
	}

	public ReservationResponse getResponse(String Response) {

		ReservationResponse ResponseObj = new ReservationResponse();
		try {
			Document doc = Jsoup.parse(Response);
			if (doc.getElementsByTag("reservationresponse").attr("status").equalsIgnoreCase("Y")) {

				logger.info("................reservationresponse reading Starting............");
				ResponseObj.setRreservationResponseStatus("Y");
				ResponseObj.setReferenceno(doc.getElementsByTag("referenceno").text().trim());
				ResponseObj.setTouroperatorname(doc.getElementsByTag("touroperatorname").text().trim());
				ResponseObj.setTourOperatorOrderNumber(doc.getElementsByTag("touroperatorordernumber").text().trim());
				ResponseObj.setBookedHotelCode(doc.getElementsByTag("hotelCode").text().trim());
				ResponseObj.setBookedHotelname(doc.getElementsByTag("hotelName").text().trim());
				ResponseObj.setReservationsource(doc.getElementsByTag("reservationsource").text().trim());
				ResponseObj.setReservationstatus(doc.getElementsByTag("reservationstatus").text().trim());
				ResponseObj.setReservationdate(doc.getElementsByTag("reservationdate").text().trim());
				ResponseObj.setCheckin(doc.getElementsByTag("checkin").text().trim());
				ResponseObj.setCheckout(doc.getElementsByTag("checkout").text().trim());
				ResponseObj.setNoOfRooms(doc.getElementsByTag("noofrooms").text().trim());
				ResponseObj.setNoofNights(doc.getElementsByTag("noofnights").text().trim());
				ResponseObj.setTotalnoofadults(doc.getElementsByTag("totalnoofadults").text().trim());
				ResponseObj.setTotalnoofchildren(doc.getElementsByTag("totalnoofchildren").text().trim());
				ResponseObj.setCurrency(doc.getElementsByTag("total").attr("currency"));
				ResponseObj.setTotal(doc.getElementsByTag("total").text().trim());
				ResponseObj.setTotalTax(doc.getElementsByTag("totaltax").text().trim());
				ResponseObj.setHotelComment(doc.getElementsByTag("hotel").text().trim());
				ResponseObj.setUserComment(doc.getElementsByTag("customer").text().trim());

				if (doc.getElementsByTag("confirmationtype").text().equalsIgnoreCase("CON")) {
					ResponseObj.setConfType(ConfirmationType.CON);
				} else if (doc.getElementsByTag("confirmationtype").text().equalsIgnoreCase("REQ")) {
					ResponseObj.setConfType(ConfirmationType.REQ);
				} else {
					ResponseObj.setConfType(ConfirmationType.NONE);
				}

				Elements roomData = doc.getElementsByTag("roomdata");
				ArrayList<Room> bookedRooomlist = new ArrayList<Room>();
				for (Element temp : roomData) {

					Room bookedRoom = new Room();
					Map<String, RateplansInfo> RatesPlanInfo = new HashMap<String, RateplansInfo>();
					RateplansInfo ratePlanObj = new RateplansInfo();

					bookedRoom.setRoomNo(temp.getElementsByTag("roomno").text());
					bookedRoom.setRoomResNo(temp.getElementsByTag("roomresno").text());
					bookedRoom.setRoomCode(temp.getElementsByTag("roomcode").text());
					bookedRoom.setRoomType(temp.getElementsByTag("roomtype").text());
					bookedRoom.setRoomTypeID(temp.getElementsByTag("roomtypecode").text());
					bookedRoom.setBedType(temp.getElementsByTag("bedtype").text());
					bookedRoom.setBedTypeID(temp.getElementsByTag("bedtypecode").text());

					ratePlanObj.setRatePlan(temp.getElementsByTag("rateplan").text());
					ratePlanObj.setRatePlanCode(temp.getElementsByTag("rateplancode").text());
					RatesPlanInfo.put(temp.getElementsByTag("roomno").text(), ratePlanObj);

					bookedRoom.setAdultsCount(temp.getElementsByTag("noofadults").text());
					bookedRoom.setChildCount(temp.getElementsByTag("noofchildren").text());

					String[] namesList = null;
					Elements guestName = doc.getElementsByTag("guest");

					try {
						namesList = new String[Integer.parseInt(temp.getElementsByTag("noofadults").text()) + Integer.parseInt(temp.getElementsByTag("noofchildren").text())];
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						logger.error("Invalid Room Adults and Child Count in the Response ", e1);
					}
					int i = 0;
					for (Element temp2 : guestName) {
						try {
							String FullName = temp2.getElementsByTag("title").text() + "_" + temp2.getElementsByTag("firstname").text() + "_" + temp2.getElementsByTag("lastname").text();
							namesList[i] = FullName;
							i++;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.error("Error inside the Guest name iterationBlock is : ", e);

						}
					}

					bookedRoom.setAdultsOccpancy(namesList);
					bookedRoom.setRatesPlanInfo(RatesPlanInfo);
					bookedRooomlist.add(bookedRoom);

				}
				ResponseObj.setRoomlist(bookedRooomlist);
				logger.info("................reservationresponse reading Ending ............");
				logger.info(" Reservation is Done , Reservation Number is :" + ResponseObj.getReferenceno());
			} else {

				logger.info("................reservationresponse reading Starting............");
				logger.info("................Reservation Status is " + doc.getElementsByTag("reservationResponse").attr("status") + "...........");
				ResponseObj.setReservationstatus(doc.getElementsByTag("reservationResponse").attr("status"));
				ResponseObj.setErrorCode(doc.getElementsByTag("code").text());
				ResponseObj.setErrorDescription(doc.getElementsByTag("description").text());
				logger.info("................Reservation ErrorDesCription is : " + doc.getElementsByTag("description").text() + "...........");

			}

		} catch (Exception e) {
			logger.fatal("Error inside the main ReaderBlock is  : ", e);
		}

		return ResponseObj;

	}

	public void buildResponse(String Path, String response) {

		try {
			org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response)));
			TransformerFactory tranFactory = TransformerFactory.newInstance();
			Transformer aTransformer = tranFactory.newTransformer();
			Source src = new DOMSource(doc);
			Result dest = new StreamResult(new File(Path));
			aTransformer.transform(src, dest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
