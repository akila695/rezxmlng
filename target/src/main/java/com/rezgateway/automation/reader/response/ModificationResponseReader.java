package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.xml.sax.InputSource;

import com.rezgateway.automation.pojo.ModificationResponse;

public class ModificationResponseReader {

	private static Logger logger = null;

	public ModificationResponseReader() {
		logger = Logger.getLogger(ModificationResponseReader.class);
	}

	public ModificationResponse getResponse(String Response) {

		ModificationResponse ResponseObj = new ModificationResponse();
		try {
			Document doc = Jsoup.parse(Response);
			logger.info("................ModifyReservationResponse reading Starting............");

			if (doc.getElementsByTag("modifyReservationResponse  ").attr("status").equalsIgnoreCase("Y")) {

				ResponseObj.setModificationResponseStatus("Y");
				ResponseObj.setReferenceNo(doc.getElementsByTag("referenceNo").text());
				ResponseObj.setNewTotalAmount(doc.getElementsByTag("newTotalAmount").text());
				ResponseObj.setModificationFee(doc.getElementsByTag("modificationFee").text());
				logger.info("...........ModifyReservation NO : " + doc.getElementsByTag("referenceNo").text());
				logger.info("................ModifyReservationResponse reading End............");

			} else {

				logger.info("................ModifyReservationResponse Status is N............");
				ResponseObj.setModificationResponseStatus("N");
				ResponseObj.setErrorCode(doc.getElementsByTag("code").text());
				ResponseObj.setErrorDescription(doc.getElementsByTag("description").text());
				logger.info("................ModifyReservationResponse reading End and Error Description is :" + doc.getElementsByTag("description").text() + "............");
			}

		} catch (Exception e) {

			logger.fatal("Error within the main ModifyReservationResponse Reader is : ", e);
		}

		logger.info("................ModifyReservationResponse Return the Response Object............");
		return ResponseObj;

	}
	
	public void buildResponse(String Path, String response) {

		try {
			org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response)));
			TransformerFactory tranFactory = TransformerFactory.newInstance();
			Transformer aTransformer = tranFactory.newTransformer();
			Source src = new DOMSource(doc);
			Result dest = new StreamResult(new File(Path));
			aTransformer.transform(src, dest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
