package com.rezgateway.automation.reader.response;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.HbsiAvailabilityRsponse;
import com.rezgateway.automation.pojo.HbsiCancellationPolicy;
import com.rezgateway.automation.pojo.HbsiRoom;
import com.rezgateway.automation.utills.DocumentBuilder;

public class HbsiAvailabilityResponseReaderTest {
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public HbsiAvailabilityResponseReaderTest(){
		logger = Logger.getLogger(HbsiAvailabilityResponseReaderTest.class);
	}
	
	@Test
	  public void readResponseData() throws JDOMException, IOException{
		String filepath = "resources/HbsiAvailWithChildResponse.xml";
		
		DocumentBuilder builder = new DocumentBuilder();
		Document doc = builder.docBuilder(filepath);
		
		HbsiAvailabilityResponseReader reader = new HbsiAvailabilityResponseReader();
		HbsiAvailabilityRsponse response = new HbsiAvailabilityRsponse();
		
		try {
			response = reader.responseReader(doc);
		} catch (Exception e) {
			logger.fatal("Data not read properly" + e);
		}
		
		for(HbsiRoom room : response.getRoomList()){
			System.out.println("Hotel code \t\t:" + room.getHotelCode());
			System.out.println("Room type  \t\t:" + room.getRoomTypeCode());
			System.out.println("Rate plan  \t\t:" + room.getRatePlanCode());
			System.out.println("Total before tax \t:" + room.getTotalBeforeTax());
			System.out.println("Total after tax \t:" + room.getTotalAfterTax());
			System.out.println("Adult count \t\t:" + room.getAdultCount());
			System.out.println("Child count \t\t:" + room.getChildCount());
			
			for (HbsiCancellationPolicy cp : room.getCp()){
				System.out.println("Policy code \t\t:" + cp.getPolicyCode());
				System.out.println("NonRefundable \t\t:" + cp.getNonRefundable());
				System.out.println("Deadline \t\t:" + cp.getDeadLine());
				System.out.println("AmountPercent unit \t:" + cp.getAmountPercentUnit());
				System.out.println("AmountPercent amnt \t:" + cp.getAmountPercentAmnt() + "\n");
			}
			
			System.out.println("========================================== \n");
		}			
		
	}

}
