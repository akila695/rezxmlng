package com.rezgateway.automation.reader.response;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.HbsiReservationResponse;
import com.rezgateway.automation.reader.request.HbsiReservationResponseReader;
import com.rezgateway.automation.utills.DocumentBuilder;

public class HbsiReservationResponseReaderTest {
	static Logger logger;
	
	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public HbsiReservationResponseReaderTest(){
		logger = Logger.getLogger(HbsiReservationResponseReaderTest.class);
	}
	
	@Test
	  public void readResponseData() throws JDOMException, IOException{
		String filepath = "resources/HbsiReservationResponse.xml";
		
		DocumentBuilder builder = new DocumentBuilder();
		Document doc = builder.docBuilder(filepath);
		
		HbsiReservationResponseReader reader = new HbsiReservationResponseReader();
		HbsiReservationResponse response = new HbsiReservationResponse();
		
		try {
			response = reader.responseReader(doc);
		} catch (Exception e) {
			logger.fatal("Data not read properly" + e);
		}
		
		reader.responseReader(doc);
		System.out.println("=========== Reservation details ==========");
		Map<String, String[]> resDetailsMap = response.getResDetails();
		for (Map.Entry<String, String[]> entry : resDetailsMap.entrySet()) {
			System.out.println("Source \t\t: " +entry.getKey());
			String[] data = entry.getValue();
			System.out.println("Res Id \t\t: " + data[0]);
			System.out.println("Res type \t: " + data[1]);
			System.out.println("Res date \t: " + data[2] + "\n");			
		}
		System.out.println("====== End of Reservation details ======= \n");
	}	
}
