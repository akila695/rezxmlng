/**
 * 
 */
package com.rezgateway.automation.reader.request;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.SynxisReservationCommitRequest;
import com.rezgateway.automation.utills.DocumentBuilder;

/**
 * @author Dinethra
 *
 */
public class SynxisReservationCommitRequestReaderTest {

	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public SynxisReservationCommitRequestReaderTest(){
		logger = Logger.getLogger(SynxisReservationCommitRequestReaderTest.class);
	}
	
	@Test
	public void readData() throws JDOMException, IOException{
	
		String filepath = "resources/SynxisResCommitRequest.xml";
		DocumentBuilder builder = new DocumentBuilder();
		Document doc = builder.docBuilder(filepath);
		
		SynxisReservationCommitRequest srcr = new SynxisReservationCommitRequest();
		SynxisReservationCommitRequestReader srcrReader = new SynxisReservationCommitRequestReader();
		
		
		try {
			srcr = srcrReader.RequestReader(doc);
		} catch (Exception e) {
			logger.fatal("Document not read properly : " + e);
		}
		
		System.out.println("Username \t: " + srcr.getUserName());
		System.out.println("Password \t: " + srcr.getPassword());
		System.out.println("Supplier ID \t: " + srcr.getSupplierID());
		System.out.println("ID Context \t: " + srcr.getIdContext());
		System.out.println("Hotel Code \t: " + srcr.getHotelCode());
	}
	
	@After
	public void afterTest(){			
		System.out.println("Test is Done");
	}

}
