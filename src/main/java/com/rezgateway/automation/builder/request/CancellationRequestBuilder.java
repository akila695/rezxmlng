package com.rezgateway.automation.builder.request;

import java.io.FileWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.rezgateway.automation.pojo.CancellationRequest;

public class CancellationRequestBuilder {

	private static Logger logger = null;

	public CancellationRequestBuilder() {
		logger = Logger.getLogger(CancellationRequestBuilder.class);
	}

	public Document getDocument(CancellationRequest reqObj) {

		Document doc = new Document();
		Element cancellationRequest = new Element("cancellationRequest");

		try {
			logger.info("........cancellationRequest_Document Starting.........."+reqObj.getScenarioID());
			cancellationRequest.setAttribute("timestamp", reqObj.getCancellationRequestTimestamp());
			doc.setRootElement(cancellationRequest);
			Element control = new Element("control");
			control.addContent(new Element("userName").setText(reqObj.getUserName()));
			control.addContent(new Element("passWord").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(control);
			cancellationRequest.addContent(new Element("supplierReferenceNo").setText(reqObj.getSupplierReferenceNo()));
			cancellationRequest.addContent(new Element("cancellationReason").setText(reqObj.getCancellationReason()));
			cancellationRequest.addContent(new Element("cancellationNotes").setText(reqObj.getCancellationNotes()));
			logger.info("........cancellationRequest_Document Ending.........."+reqObj.getScenarioID());
			
		} catch (Exception e) {
			logger.fatal("Error While Building Request :"+reqObj.getScenarioID(), e);
		}

		return doc;
	}

	public String buildRequest(String path, CancellationRequest reqObj) {

		String out = "Not set the Documnet correctly";
		StringWriter rw = new StringWriter();
		Document xmlDoc = new Document();
		try {

			xmlDoc = getDocument(reqObj);
			XMLOutputter xmlOut = new XMLOutputter();
			xmlOut.setFormat(Format.getPrettyFormat());
			xmlOut.output(xmlDoc, new FileWriter(path));
			xmlOut.output(xmlDoc, rw);
			out = rw.getBuffer().toString();

		} catch (Exception es) {
			logger.fatal("Error While Building Request :"+reqObj.getScenarioID(), es);

		}

		return out;
	}

	public String buildRequest(CancellationRequest reqObj) {

		String out = null;
		StringWriter sw = new StringWriter();
		Document xmlDoc = new Document();
		try {
			xmlDoc = getDocument(reqObj);
			XMLOutputter xmlout = new XMLOutputter();
			xmlout.setFormat(Format.getPrettyFormat());
			xmlout.output(xmlDoc, sw);
			out = sw.getBuffer().toString();

		} catch (Exception e) {
			logger.fatal("Error While Building Request :"+reqObj.getScenarioID(), e);

		}

		return out;
	}

}
