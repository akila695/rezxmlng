/**
 * 
 */
package com.rezgateway.automation.reader.request;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import com.rezgateway.automation.pojo.SynxisReservationCommitRequest;

/**
 * @author Dinethra
 *
 */
public class SynxisReservationCommitRequestReader {
	
	static Logger logger;
	
	public SynxisReservationCommitRequestReader() {
		logger = Logger.getLogger(SynxisReservationCommitRequestReader.class);
	}
	
	Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
	Namespace ns1 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
	Namespace ns2 = Namespace.getNamespace("http://htng.org/1.1/Header/");
	
	public SynxisReservationCommitRequest RequestReader(Document doc){
		
		logger.info("Initializing response reader with ==>" + doc.toString());
		
		SynxisReservationCommitRequest srcr = new SynxisReservationCommitRequest();
		
		try {
			Element root = doc.getRootElement();
			Element header = root.getChild("Header", ns);
			Element htng = header.getChild("HTNGHeader", ns2);
			Element from = htng.getChild("From", ns2);
			Element credentials = from.getChild("Credential", ns2);
			srcr.setUserName(credentials.getChildText("userName", ns2));
			srcr.setPassword(credentials.getChildText("password", ns2));
			Element body = root.getChild("Body", ns);
			Element ota_HotelResRQ = body.getChild("OTA_HotelResRQ", ns1);
			Element hReservations = ota_HotelResRQ.getChild("HotelReservations", ns1);
			Element hreserv = hReservations.getChild("HotelReservation", ns1);
			srcr.setSupplierID(hreserv.getChild("UniqueID", ns1).getAttributeValue("ID"));
			srcr.setIdContext(hreserv.getChild("UniqueID", ns1).getAttributeValue("ID_Context"));
			Element roomStays = hreserv.getChild("RoomStays", ns1);
			Element roomStay = roomStays.getChild("RoomStay", ns1);
			srcr.setHotelCode(Integer.parseInt(roomStay.getChild("BasicPropertyInfo", ns1).getAttributeValue("HotelCode")));
		} catch (Exception e) {
			logger.fatal("Error in reading commit reservation request : " + e);
		}
		return srcr;
		
	}

}
