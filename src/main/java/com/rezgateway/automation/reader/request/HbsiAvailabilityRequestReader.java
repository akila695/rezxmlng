package com.rezgateway.automation.reader.request;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import com.rezgateway.automation.pojo.HbsiAvailabilityRequest;

public class HbsiAvailabilityRequestReader {
	static Logger logger;
	
	public HbsiAvailabilityRequestReader() {
		logger = Logger.getLogger(HbsiAvailabilityRequestReader.class);
	}
	
	public HbsiAvailabilityRequest RequestReader(Document doc) throws JDOMException, IOException {
		logger.info("Initializing request reader with ==>" + doc.toString());
		HbsiAvailabilityRequest req = new HbsiAvailabilityRequest();
		Element rootElem = doc.getRootElement();
				
		Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
		Element body = rootElem.getChild("Body", ns);
		
		Namespace ns1 = Namespace.getNamespace("http://www.demandmatrix.net/HBSIXML4/");
		Element xml = body.getChild("GetSoapRequest", ns1).getChild("Message", ns1).getChild("XML", ns1);
		String availReq = xml.getText();
				
		SAXBuilder builder = new SAXBuilder();
		InputStream stream = new ByteArrayInputStream(availReq.getBytes("UTF-8"));
		Document docReq = builder.build(stream);
		
		Element rootElemReq = docReq.getRootElement();
		Namespace ns2 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
		
		Element AvailRequestSegment = rootElemReq.getChild("AvailRequestSegments", ns2).getChild("AvailRequestSegment", ns2);
		Element Criterion = AvailRequestSegment.getChild("HotelSearchCriteria", ns2).getChild("Criterion", ns2);
				
		req.setCheckin(Criterion.getChild("StayDateRange", ns2).getAttributeValue("Start"));
		req.setCheckout(Criterion.getChild("StayDateRange", ns2).getAttributeValue("End"));
		
		Element ratePlanCandidates = Criterion.getChild("RatePlanCandidates", ns2);
		List<Element> ratePlanCandidate = ratePlanCandidates.getChildren("RatePlanCandidate", ns2);
		
		Map<String, String[]> ratePlanCandidateMap = new HashMap<String, String[]>();
		 			
		logger.info("Reading rate plan details");
 		
 		try {
			for (Element rateplan : ratePlanCandidate) {
				String rateCode = rateplan.getAttributeValue("RatePlanCode");
				String RPH = rateplan.getAttributeValue("RPH");
								
				Element hotelRefs = rateplan.getChild("HotelRefs", ns2);
				String hotelCode = hotelRefs.getChild("HotelRef", ns2).getAttributeValue("HotelCode");
				
				Element mealPlan = rateplan.getChild("MealsIncluded", ns2);
				String mealCode = mealPlan.getAttributeValue("MealPlanCodes");
				ratePlanCandidateMap.put(RPH, new String[] {hotelCode, rateCode, mealCode});
				req.setRatePlanCandidate(ratePlanCandidateMap);								
			}
		} catch (Exception e) {
			logger.error("Eror in reading rate plan details : " + e);
		}
 		
 		Element roomStayCandidates = Criterion.getChild("RoomStayCandidates", ns2);
 		List<Element> roomStayCandidate = roomStayCandidates.getChildren("RoomStayCandidate", ns2);
 		
 		Map<String, String[]> roomStayMap = new HashMap<String, String[]>();
			
		logger.info("Reading roomStay details");
		try {
			for(Element roomStay : roomStayCandidate){
				String RPH = roomStay.getAttributeValue("RPH");
				String roomTypeCode = roomStay.getAttributeValue("RoomTypeCode");
				String qty = roomStay.getAttributeValue("Quantity");
				 
				Element guestCounts = roomStay.getChild("GuestCounts", ns2);
				List<Element> guestCount = guestCounts.getChildren("GuestCount", ns2);
				
				logger.info("Reading occupancy count");
				
				String adultCount = null;
				String childCount = null;
		 		
		 		try {
					for (Element guest : guestCount) {
						String ageCode = guest.getAttributeValue("AgeQualifyingCode");

						if ("10".equals(ageCode)) {
							adultCount = guest.getAttributeValue("Count");							
						} else if ("8".equals(ageCode)) {
							childCount = guest.getAttributeValue("Count");							
						}
						roomStayMap.put(RPH, new String[] {roomTypeCode, qty, adultCount, childCount});
						req.setRoomStayCandidate(roomStayMap);
					}					
				} catch (Exception e) {
					logger.error("Error in reading occupancy details");
				}
			}			
		} catch (Exception e) {
			logger.error("Eror in reading room stay details : " + e);
		}
	
		return req;		
	}
}
