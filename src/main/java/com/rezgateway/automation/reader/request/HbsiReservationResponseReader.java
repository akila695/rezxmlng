package com.rezgateway.automation.reader.request;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import com.rezgateway.automation.pojo.HbsiReservationResponse;

public class HbsiReservationResponseReader {
static Logger logger;
	
	public HbsiReservationResponseReader() {
		logger = Logger.getLogger(HbsiReservationResponseReader.class);
	}
	
	public HbsiReservationResponse responseReader(Document doc) throws JDOMException, IOException {
		logger.info("Initializing response reader with ==>" + doc.toString());
		HbsiReservationResponse response = new HbsiReservationResponse();
		Element rootElem = doc.getRootElement();
		
		Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
		Namespace ns1 = Namespace.getNamespace("http://www.demandmatrix.net/HBSIXML4/");
		Namespace ns2 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
		
		Element body = rootElem.getChild("Body", ns);
		String responseXml = body.getChild("GetSoapRequestResponse", ns1).getChildText("GetSoapRequestResult", ns1);
		
		SAXBuilder builder = new SAXBuilder();
		InputStream stream = new ByteArrayInputStream(responseXml.getBytes("UTF-8"));
		Document docReq = builder.build(stream);
		
		Element rootElemReq = docReq.getRootElement();
		Element hotelRes = rootElemReq.getChild("HotelReservations", ns2).getChild("HotelReservation", ns2);
		Element hotelResIDs = hotelRes.getChild("ResGlobalInfo", ns2).getChild("HotelReservationIDs", ns2);
		List<Element> resIdList = hotelResIDs.getChildren("HotelReservationID", ns2);
		
		Map<String, String[]> resDetailsMap = new HashMap<String, String[]>();
		logger.info("Reading reservation details");
		try {
			for (Element resID : resIdList){
				String source = resID.getAttributeValue("ResID_Source");
				String id = resID.getAttributeValue("ResID_Value");
				String type = resID.getAttributeValue("ResID_Type");
				String date = resID.getAttributeValue("ResID_Date");
				
				resDetailsMap.put(source, new String[] {id, type, date});
				response.setResDetails(resDetailsMap);
			}			
		} catch (Exception e) {
			logger.error("Eror in reading reservation details : " + e);
		}		
				
		return response;
	}

}
