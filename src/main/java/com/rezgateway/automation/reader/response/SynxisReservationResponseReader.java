/**
 * 
 */
package com.rezgateway.automation.reader.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import com.rezgateway.automation.pojo.Occupancy;
import com.rezgateway.automation.pojo.SynxisCancellationPolicy;
import com.rezgateway.automation.pojo.SynxisReservationResponse;

/**
 * @author Dinethra
 *
 */
public class SynxisReservationResponseReader{
	
	static Logger logger;
	
	private Namespace ns  = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
	private Namespace ns2 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
	private ArrayList<String> resID_Value = new ArrayList<String>();
	
	public SynxisReservationResponseReader() {
		logger = Logger.getLogger(SynxisReservationResponseReader.class);
	}
	
	public SynxisReservationResponse RequestReader(Document doc){
		logger.info("Initializing response reader with ==>" + doc.toString());
		
		SynxisReservationResponse srr = new SynxisReservationResponse();
		Occupancy occupant = new Occupancy();
		SynxisCancellationPolicy scp = new SynxisCancellationPolicy();
		String ageQCode = null, resIDValue = null;
		
		Element root = doc.getRootElement();
		Element body = root.getChild("Body", ns);
		
		Element ota_HotelResRS = body.getChild("OTA_HotelResRS", ns2);
		srr.setResResponseType(ota_HotelResRS.getAttributeValue("ResResponseType"));

		Element hotelReservations = ota_HotelResRS.getChild("HotelReservations", ns2);
		Element hotelReservation = hotelReservations.getChild("HotelReservation", ns2);
		Element uniqueID = hotelReservation.getChild("UniqueID", ns2);
		srr.setSupplierID(uniqueID.getAttributeValue("ID"));
		srr.setSidContext(uniqueID.getAttributeValue("ID_Context"));
		
		Element roomStays = hotelReservation.getChild("RoomStays", ns2);
		Element roomStay = roomStays.getChild("RoomStay", ns2);
		srr.setGuranteeCode(roomStay.getChild("Guarantee", ns2).getAttributeValue("GuaranteeCode"));
		
		srr.setTotalBeforeTax(Double.parseDouble(roomStay.getChild("Total", ns2).getAttributeValue("AmountBeforeTax")));
		srr.setTotalAfterTax(Double.parseDouble(roomStay.getChild("Total", ns2).getAttributeValue("AmountAfterTax")));
		srr.setCurrencyCode(roomStay.getChild("Total", ns2).getAttributeValue("CurrencyCode"));
		
		logger.info("Reading room details");
		
		Element roomTypes = roomStay.getChild("RoomTypes", ns2);
		Element roomType = roomTypes.getChild("RoomType", ns2);
		srr.setRoomTypeCode(roomType.getAttributeValue("RoomTypeCode"));
		srr.setNumberOfUnits(Integer.parseInt(roomType.getAttributeValue("NumberOfUnits")));
		srr.setRoomDesciption(roomType.getChild("RoomDescription", ns2).getAttributeValue("Name"));
		srr.setRoomMaxOccupancy(Integer.parseInt(roomType.getChild("Occupancy", ns2).getAttributeValue("MaxOccupancy")));
		
		logger.info("Reading rate plan details");
		
		Element ratePlans = roomStay.getChild("RatePlans", ns2);
		Element ratePlan = ratePlans.getChild("RatePlan", ns2);
		srr.setRatePlanCode(ratePlan.getAttributeValue("RatePlanCode"));
		srr.setRatePlanName(ratePlan.getAttributeValue("RatePlanName"));
		
		logger.info("Reading cancellation policy details");
		
		Element cancelPenalties = ratePlan.getChild("CancelPenalties", ns2);
		Element cancelPenalty = cancelPenalties.getChild("CancelPenalty", ns2);
		scp.setPolicyCode(cancelPenalty.getAttributeValue("PolicyCode"));
		
		scp.setPenltyDesc(cancelPenalty.getChild("PenaltyDescription", ns2).getChildText("Text", ns2));
		Element amountPercent = cancelPenalty.getChild("AmountPercent", ns2);
		scp.setIsTaxInclusive(amountPercent.getAttributeValue("TaxInclusive"));
		
		try {
			if (amountPercent.getAttribute("NmbrOfNights") != null) {
				scp.setAmount(amountPercent.getAttributeValue("NmbrOfNights")+ " Nights");
			} 
			else if (amountPercent.getAttribute("Percent") != null) {
				scp.setAmount(amountPercent.getAttributeValue("Percent") + "%");
			} 
			else if (amountPercent.getAttribute("Amount") != null) {
				scp.setAmount(amountPercent.getAttributeValue("CurrencyCode")+ " " + amountPercent.getAttributeValue("Amount"));
			}
		} catch (Exception e) {
			logger.error("Not a proper cancellation type : "+ e);
		}
					
		Element deadline = cancelPenalty.getChild("Deadline", ns2);
		
		try {
			if (deadline.getAttribute("OffsetTimeUnit") != null)
				scp.setOffsetTimeUnit(deadline.getAttributeValue("OffsetTimeUnit"));

			if (deadline.getAttribute("OffsetUnitMultiplier") != null)
				scp.setOffsetUnitMultiplier(deadline.getAttributeValue("OffsetUnitMultiplier"));

			if (deadline.getAttribute("OffsetDropTime") != null)
				scp.setOffsetDropTime(deadline.getAttributeValue("OffsetDropTime"));

			if (deadline.getAttribute("AbsoluteDeadline") != null)
				scp.setDeadLine(deadline.getAttributeValue("AbsoluteDeadline"));
		}
		catch (Exception e) {
			logger.error("Issue in other tags in cancellation policy : " + e);
		}
		
		srr.setsCancelPolicy(scp);
		
		logger.info("Reading base and discount amounts");
		
		Element roomRates = roomStay.getChild("RoomRates", ns2);
		Element roomRate = roomRates.getChild("RoomRate", ns2);
		Element rates = roomRate.getChild("Rates", ns2);
		Element rate = rates.getChild("Rate", ns2);
		
		srr.setBaseAmountBeforeTax(Double.parseDouble(rate.getChild("Base", ns2).getAttributeValue("AmountBeforeTax")));
		srr.setBaseAmountAfterTax(Double.parseDouble(rate.getChild("Base", ns2).getAttributeValue("AmountAfterTax")));
		
		try{
			if(rate.getChild("Discount", ns2)!=null){
				srr.setDiscountAmountBeforeTax(Double.parseDouble(rate.getChild("Discount", ns2).getAttributeValue("AmountBeforeTax")));
				srr.setDiscountAmountAfterTax(Double.parseDouble(rate.getChild("Discount", ns2).getAttributeValue("AmountAfterTax")));		
			}
		}catch(Exception e){
			logger.fatal("Discount details are not properly read : " +e);
		}
		
		logger.info("Reading fees and tax amounts");
		
		Element fees = rate.getChild("Fees", ns2);
		srr.setFeeAmount(Double.parseDouble(fees.getChild("Fee", ns2).getAttributeValue("Amount")));
		
		Element taxes = rate.getChild("Taxes", ns2);
		srr.setTax(Double.parseDouble(taxes.getChild("Tax", ns2).getAttributeValue("Amount")));
		
		logger.info("Reading nightly rates");
		
		List<Element> nightlyRate = rate.getChild("Tpa_Extensions", ns2).getChildren("NightlyRate", ns2);
		Map<String, String[]> nightlyRateMap = new HashMap<String, String[]>();

		try {
			for (Element nr : nightlyRate) {

				String date = nr.getAttributeValue("Date");
				String price = nr.getAttributeValue("Price");
				String nrTax = nr.getAttributeValue("Tax");
				String fee = nr.getAttributeValue("Fee");
				String pwtaf = nr.getAttributeValue("PriceWithTaxAndFee");

				nightlyRateMap.put(date, new String[] { price, nrTax, fee,
						pwtaf });
				srr.setNightlyRate(nightlyRateMap);
			}
		} catch (Exception e) {
			logger.fatal("Nightly rates not properly read : " + e);
		}
		
		try{
			if(rate.getChild("Tpa_Extensions", ns2).getChildren("DiscountedNightlyRate", ns2)!=null){
			
				logger.info("Reading discount nightly rates");
				
				List<Element> discountedNightlyRate = rate.getChild("Tpa_Extensions", ns2).getChildren("DiscountedNightlyRate", ns2);
				Map<String, String[]> discountedNightlyRateMap = new HashMap<String, String[]>();
				for (Element dnr : discountedNightlyRate) {
				
					String ddate = dnr.getAttributeValue("Date");
					String dprice = dnr.getAttributeValue("Price");
					String dnrTax = dnr.getAttributeValue("Tax");
					String dfee = dnr.getAttributeValue("Fee");
					String dpwtaf = dnr.getAttributeValue("PriceWithTaxAndFee");
				
					discountedNightlyRateMap.put(ddate, new String[] {dprice, dnrTax, dfee, dpwtaf});
					srr.setDiscountNightlyRate(discountedNightlyRateMap);
				}
			}
		} catch (Exception e){
			logger.fatal("Issue in discount nightly rates : "+ e);
		}
		
		logger.info("Reading occupant count");
		
		Element guestCounts = roomStay.getChild("GuestCounts", ns2);
		List<Element> guestCount = guestCounts.getChildren("GuestCount", ns2);
 		
		try {
			for (Element guest : guestCount) {
				ageQCode = guest.getAttributeValue("AgeQualifyingCode");

				if ("10".equals(ageQCode))
					srr.setAdultCount(Integer.parseInt(guest.getAttributeValue("Count")));
				else if ("8".equals(ageQCode))
					srr.setChildCount(Integer.parseInt(guest.getAttributeValue("Count")));
			}
		} catch (Exception e) {
			logger.fatal("Age qualifying code is wrong : "+ e);
		}
		
		logger.info("Reading reservation details");
		
		srr.setCheckin(roomStay.getChild("TimeSpan", ns2).getAttributeValue("Start"));
		srr.setCheckout(roomStay.getChild("TimeSpan", ns2).getAttributeValue("End"));
		srr.setDuration(roomStay.getChild("TimeSpan", ns2).getAttributeValue("Duration"));
		srr.setHotelCode(Integer.parseInt(roomStay.getChild("BasicPropertyInfo", ns2).getAttributeValue("HotelCode")));
		srr.setHotelName(roomStay.getChild("BasicPropertyInfo", ns2).getAttributeValue("HotelName"));
		
		Element resGuests = hotelReservation.getChild("ResGuests", ns2);
 		List<Element> resGuest = resGuests.getChildren("ResGuest", ns2);
 		ArrayList<Occupancy> occu = new ArrayList<Occupancy>();
 		
 		logger.info("Reading occupant details");
 		
 		try {
			for (Element guest : resGuest) {

				Element profiles = guest.getChild("Profiles", ns2);
				Element profileInfo = profiles.getChild("ProfileInfo", ns2);
				Element profile = profileInfo.getChild("Profile", ns2);
				Element customer = profile.getChild("Customer", ns2);
				Element personName = customer.getChild("PersonName", ns2);

				occupant.setNamePrefix(personName.getChildText("NamePrefix", ns2));
				occupant.setFirstName(personName.getChildText("GivenName", ns2));
				occupant.setLastName(personName.getChildText("Surname", ns2));

				Element telephone = customer.getChild("Telephone", ns2);

				occupant.setTelephone(telephone.getAttributeValue("PhoneNumber"));
				occupant.setEmail(customer.getChildText("Email", ns2));

				Element address = customer.getChild("Address", ns2);

				occupant.setAddressLine(address	.getChildText("AddressLine", ns2));
				occupant.setCity(address.getChildText("CityName", ns2));
				occupant.setPostalCode(Integer.parseInt(address.getChildText("PostalCode", ns2)));
				occupant.setStateCode(address.getChild("StateProv", ns2).getAttributeValue("StateCode"));
				occupant.setCountryCode(address.getChild("CountryName", ns2).getAttributeValue("Code"));
				
				occu.add(occupant);			
			}
			srr.setOccupant(occu);		
		} catch (Exception e) {
			logger.fatal("Occupnacy details are not properly read : " + e);
		}
 		
 		logger.info("Reading credit card details");
 		
		Element resGlobalInfo = hotelReservation.getChild("ResGlobalInfo", ns2);
 		Element guarantee = resGlobalInfo.getChild("Guarantee", ns2);
 		Element guaranteesAccepted = guarantee.getChild("GuaranteesAccepted", ns2);
 		Element guaranteeAccepted = guaranteesAccepted.getChild("GuaranteeAccepted", ns2);
 		Element paymentCard = guaranteeAccepted.getChild("PaymentCard", ns2);
 		
 		srr.setCardcode(paymentCard.getAttributeValue("CardCode"));
 		srr.setCardNumber(paymentCard.getAttributeValue("CardNumber"));
 		srr.setExpireDate(paymentCard.getAttributeValue("ExpireDate"));
 		srr.setCardHolderName(paymentCard.getChildText("CardHolderName", ns2));
 		
 		logger.info("Reading hotel reservation IDs");
 		
		Element hotelReservationIDs = resGlobalInfo.getChild("HotelReservationIDs", ns2);
		List<Element> hotelReservationID = hotelReservationIDs.getChildren("HotelReservationID", ns2);
 		
 		try {
			for (Element hresID : hotelReservationID) {
				resIDValue = hresID.getAttributeValue("ResID_Value");
				resID_Value.add(resIDValue);
			}
		} catch (Exception e) {
			logger.fatal("Hotel reservation IDs are not read properly : " + e);
		}
		srr.setHotelReservationID(resID_Value);		
		return srr;
	}

}
