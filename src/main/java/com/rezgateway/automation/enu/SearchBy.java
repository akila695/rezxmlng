package com.rezgateway.automation.enu;

public enum SearchBy {
	HOTELCODE,IATACODE,HOTELGROUPCODE,CITYCODE,NONE;
	
	
	public static SearchBy getSearchByType(String type){
		
		if(type.trim().equalsIgnoreCase("HOTELCODE")){
			return SearchBy.HOTELCODE;
		}else if (type.trim().equalsIgnoreCase("IATACODE")){
			return SearchBy.IATACODE;
		}else if(type.trim().equalsIgnoreCase("HOTELGROUPCODE")){
			return SearchBy.HOTELGROUPCODE;
		}else if (type.trim().equalsIgnoreCase("CITYCODE")){
			return SearchBy.CITYCODE;
		}else {
			return SearchBy.NONE;
		}
		
	}
}
