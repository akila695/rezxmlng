package com.rezgateway.automation.pojo;
/**
 * @author Akila
 *
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Hotel {
	
	private String 							HotelCode 							= null;
	private String 							Name 								= null;
	private String 							Address 							= null;
	private String 							City 								= null;
	private String 							StateProvince 						= null;
	private String 							Country 							= null;
	private String 							PostalCode 							= null;
	private String 							RateCurrencyCode 					= null;
	private String 							ShortDescription 					= null;
	private String 							StarRating 							= null;
	private String 							ThumbNailUrl 						= null;
	private String 							HotelUrl 							= null;
	private String 							Maintenance 						= null;
	private ArrayList<BookingPolicy>		hotelBookingPolicy					= new ArrayList<BookingPolicy>();
	private String 							PolicyDescription 					= null;
	private Map<String, ArrayList<Room>> 	roomInfo 							= new HashMap<String, ArrayList<Room>>();
	/**
	 * @return the hotelCode
	 */
	public String getHotelCode() {
		return HotelCode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return Address;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return City;
	}
	/**
	 * @return the stateProvince
	 */
	public String getStateProvince() {
		return StateProvince;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return Country;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return PostalCode;
	}
	/**
	 * @return the rateCurrencyCode
	 */
	public String getRateCurrencyCode() {
		return RateCurrencyCode;
	}
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return ShortDescription;
	}
	/**
	 * @return the starRating
	 */
	public String getStarRating() {
		return StarRating;
	}
	/**
	 * @return the thumbNailUrl
	 */
	public String getThumbNailUrl() {
		return ThumbNailUrl;
	}
	/**
	 * @return the hotelUrl
	 */
	public String getHotelUrl() {
		return HotelUrl;
	}
	/**
	 * @return the maintenance
	 */
	public String getMaintenance() {
		return Maintenance;
	}
	
	/**
	 * @return the policyDescription
	 */
	public String getPolicyDescription() {
		return PolicyDescription;
	}
	/**
	 * @return the roomInfo
	 */
	public Map<String, ArrayList<Room>> getRoomInfo() {
		return roomInfo;
	}
	/**
	 * @param hotelCode the hotelCode to set
	 */
	public void setHotelCode(String hotelCode) {
		HotelCode = hotelCode;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		Address = address;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		City = city;
	}
	/**
	 * @param stateProvince the stateProvince to set
	 */
	public void setStateProvince(String stateProvince) {
		StateProvince = stateProvince;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		Country = country;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	/**
	 * @param rateCurrencyCode the rateCurrencyCode to set
	 */
	public void setRateCurrencyCode(String rateCurrencyCode) {
		RateCurrencyCode = rateCurrencyCode;
	}
	/**
	 * @param shortDescription the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		ShortDescription = shortDescription;
	}
	/**
	 * @param starRating the starRating to set
	 */
	public void setStarRating(String starRating) {
		StarRating = starRating;
	}
	/**
	 * @param thumbNailUrl the thumbNailUrl to set
	 */
	public void setThumbNailUrl(String thumbNailUrl) {
		ThumbNailUrl = thumbNailUrl;
	}
	/**
	 * @param hotelUrl the hotelUrl to set
	 */
	public void setHotelUrl(String hotelUrl) {
		HotelUrl = hotelUrl;
	}
	/**
	 * @param maintenance the maintenance to set
	 */
	public void setMaintenance(String maintenance) {
		Maintenance = maintenance;
	}
	/**
	 * @param policyDescription the policyDescription to set
	 */
	public void setPolicyDescription(String policyDescription) {
		PolicyDescription = policyDescription;
	}
	/**
	 * @param roomInfo the roomInfo to set
	 */
	public void setRoomInfo(Map<String, ArrayList<Room>> roomInfo) {
		this.roomInfo = roomInfo;
	}
	/**
	 * @return the hotelBookingPolicy
	 */
	public ArrayList<BookingPolicy> getHotelBookingPolicy() {
		return hotelBookingPolicy;
	}
	/**
	 * @param hotelBookingPolicy the hotelBookingPolicy to set
	 */
	public void setHotelBookingPolicy(ArrayList<BookingPolicy> hotelBookingPolicy) {
		this.hotelBookingPolicy = hotelBookingPolicy;
	}
	
	
 
}
