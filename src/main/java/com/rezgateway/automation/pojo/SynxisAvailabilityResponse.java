package com.rezgateway.automation.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SynxisAvailabilityResponse {

	private String echoToken		= null;
	private String hotelCode		= null;
	private String hotelName 		= null;
	private ArrayList<SynxisRoom> roomList	= new ArrayList<SynxisRoom>();
	private Map<String, String[]> roomTypes			= new HashMap<String, String[]>();
	private Map<String, String[]> ratePlans			= new HashMap<String, String[]>();
	
	public Map<String, String[]> getRoomTypes() {
		return roomTypes;
	}
	
	public void setRoomTypes(Map<String, String[]> roomTypes) {
		this.roomTypes = roomTypes;
	}
	
	public Map<String, String[]> getRatePlans() {
		return ratePlans;
	}
	
	public void setRatePlans(Map<String, String[]> ratePlans) {
		this.ratePlans = ratePlans;
	}
	
	public ArrayList<SynxisRoom> getRoomList() {
		return roomList;
	}
	
	public void setRoomList(ArrayList<SynxisRoom> roomList) {
		this.roomList = roomList;
	}
	
	public String getEchoToken() {
		return echoToken;
	}
	
	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}
	
	public String getHotelCode() {
		return hotelCode;
	}
	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}
	
	public String getHotelName() {
		return hotelName;
	}
	
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

}
