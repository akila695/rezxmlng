/**
 * 
 */
package com.rezgateway.automation.pojo;

import java.util.ArrayList;


/**
 * @author Dinethra
 * 
 */

public class SynxisReservationInitiateRequest {

	private String username = null;
	private String password = null;
	private String roomCode = null;
	private int roomCount = 0;
	private String ratePlanCode = null;
	private int adultCount = 0;
	private int childCount = 0;
	private String checkin = null;
	private String checkout = null;
	private String hotelCode = null;
	private String cardcode = null;
	private String cardNumber = null;
	private int seriesCode = 0;
	private String expireDate = null;
	private String cardHolderName = null;
	private ArrayList<Occupancy> occupant = new ArrayList<Occupancy>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public int getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(int roomCount) {
		this.roomCount = roomCount;
	}

	public String getRatePlanCode() {
		return ratePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public String getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	public String getCardcode() {
		return cardcode;
	}

	public void setCardcode(String cardcode) {
		this.cardcode = cardcode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(int seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public ArrayList<Occupancy> getOccupant() {
		return occupant;
	}

	public void setOccupant(ArrayList<Occupancy> occupant) {
		this.occupant = occupant;
	}

}
