package com.rezgateway.automation.pojo;

import java.util.ArrayList;

import com.rezgateway.automation.enu.ConfirmationType;

public class ReservationRequest extends AvailabilityRequest {

	private String 						ReservationDetailsTimeStamp 				= null;
	private String 						TourOperatorOrderNumber 					= null;
	private String 						Total 										= null;
	private String 						TotalTax 									= null;
	private String 						Currency 									= null;
	private ArrayList<Room>				RezRoomList 								= new ArrayList<Room>();
	private ConfirmationType 			ConfType 									= ConfirmationType.CON;
	private String 						UserComment									= "No comments available";
	private String 						HotelComment 								= "No comments available";
	private String 						ReturnCompeleteBookingDetails 				= "Y";
	
	
	
	public String getReturnCompeleteBookingDetails() {
		return ReturnCompeleteBookingDetails;
	}

	public void setReturnCompeleteBookingDetails(String returnCompeleteBookingDetails) {
		ReturnCompeleteBookingDetails = returnCompeleteBookingDetails;
	}

	public String getReservationDetailsTimeStamp() {
		return ReservationDetailsTimeStamp;
	}

	public void setReservationDetailsTimeStamp(String reservationDetailsTimeStamp) {
		ReservationDetailsTimeStamp = reservationDetailsTimeStamp;
	}

	public String getTourOperatorOrderNumber() {
		return TourOperatorOrderNumber;
	}

	public void setTourOperatorOrderNumber(String tourOperatorOrderNumber) {
		TourOperatorOrderNumber = tourOperatorOrderNumber;
	}

	public String getTotal() {
		return Total;
	}

	public void setTotal(String total) {
		Total = total;
	}

	public String getTotalTax() {
		return TotalTax;
	}

	public void setTotalTax(String totalaTax) {
		TotalTax = totalaTax;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public ArrayList<Room> getRezRoomList() {
		return RezRoomList;
	}

	public void setRezRoomList(ArrayList<Room> roomList) {
		RezRoomList = roomList;
	}
	
	public void addToRezRoomList(Room rm) {
		RezRoomList.add(rm);
	}


	public String getUserComment() {
		return UserComment;
	}

	public void setUserComment(String userComment) {
		UserComment = userComment;
	}

	public String getHotelComment() {
		return HotelComment;
	}

	public void setHotelComment(String hotelComment) {
		HotelComment = hotelComment;
	}

	public ConfirmationType getConfType() {
		return ConfType;
	}

	public void setConfType(ConfirmationType confType) {
		ConfType = confType;
	}

}
