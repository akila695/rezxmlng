package com.rezgateway.automation.pojo;

import java.util.ArrayList;

public class HbsiAvailabilityRsponse {
	private ArrayList<HbsiRoom> roomList	= new ArrayList<HbsiRoom>();

	public ArrayList<HbsiRoom> getRoomList() {
		return roomList;
	}

	public void setRoomList(ArrayList<HbsiRoom> roomList) {
		this.roomList = roomList;
	}	

}
