package com.rezgateway.automation.pojo;

import java.util.ArrayList;

public class HbsiReservationRequest {
	private String uniqueId			= null;
	private String checkin 			= null;
	private String checkout 		= null;
	private String cardCode			= null;
	private String cardType			= null;
	private String expireDate		= null;
	private String cardNo			= null;
	private String seriesCode		= null;
	private String cardHolderName 	= null;
	private ArrayList<HbsiRoom> roomList	= new ArrayList<HbsiRoom>();
	private ArrayList<Occupancy> occupant = new ArrayList<Occupancy>();
	
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getSeriesCode() {
		return seriesCode;
	}
	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public ArrayList<HbsiRoom> getRoomList() {
		return roomList;
	}
	public void setRoomList(ArrayList<HbsiRoom> roomList) {
		this.roomList = roomList;
	}
	public ArrayList<Occupancy> getOccupant() {
		return occupant;
	}
	public void setOccupant(ArrayList<Occupancy> occupant) {
		this.occupant = occupant;
	}
	
	
}
