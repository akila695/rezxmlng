package com.rezgateway.automation.pojo;

public class HotelFees {
	
	private String FeeType = "";
	private String FeeMethod = "";
	private String RequiredFee = "";
	private String FeeAssign = "";
	private String FeeFrequency = "";
	private String FeeBasedOn = "";
	private double FeeBasedOnValue = 0.0;
	private double SalesTax = 0.0;
	private String Conditions = null;
	private double FeeTotal = 0.0;
	/**
	 * @return the feeType
	 */
	public String getFeeType() {
		return FeeType;
	}
	/**
	 * @param feeType the feeType to set
	 */
	public void setFeeType(String feeType) {
		FeeType = feeType;
	}
	/**
	 * @return the feeMethod
	 */
	public String getFeeMethod() {
		return FeeMethod;
	}
	/**
	 * @param feeMethod the feeMethod to set
	 */
	public void setFeeMethod(String feeMethod) {
		FeeMethod = feeMethod;
	}
	/**
	 * @return the requiredFee
	 */
	public String getRequiredFee() {
		return RequiredFee;
	}
	/**
	 * @param requiredFee the requiredFee to set
	 */
	public void setRequiredFee(String requiredFee) {
		RequiredFee = requiredFee;
	}
	/**
	 * @return the feeAssign
	 */
	public String getFeeAssign() {
		return FeeAssign;
	}
	/**
	 * @param feeAssign the feeAssign to set
	 */
	public void setFeeAssign(String feeAssign) {
		FeeAssign = feeAssign;
	}
	/**
	 * @return the feeFrequency
	 */
	public String getFeeFrequency() {
		return FeeFrequency;
	}
	/**
	 * @param feeFrequency the feeFrequency to set
	 */
	public void setFeeFrequency(String feeFrequency) {
		FeeFrequency = feeFrequency;
	}
	/**
	 * @return the feeBasedOn
	 */
	public String getFeeBasedOn() {
		return FeeBasedOn;
	}
	/**
	 * @param feeBasedOn the feeBasedOn to set
	 */
	public void setFeeBasedOn(String feeBasedOn) {
		FeeBasedOn = feeBasedOn;
	}
	/**
	 * @return the feeBasedOnValue
	 */
	public Double getFeeBasedOnValue() {
		return FeeBasedOnValue;
	}
	/**
	 * @param feeBasedOnValue the feeBasedOnValue to set
	 */
	public void setFeeBasedOnValue(double feeBasedOnValue) {
		FeeBasedOnValue = feeBasedOnValue;
	}
	/**
	 * @return the salesTax
	 */
	public double getSalesTax() {
		return SalesTax;
	}
	/**
	 * @param salesTax the salesTax to set
	 */
	public void setSalesTax(double salesTax) {
		SalesTax = salesTax;
	}
	/**
	 * @return the conditions
	 */
	public String getConditions() {
		return Conditions;
	}
	/**
	 * @param conditions the conditions to set
	 */
	public void setConditions(String conditions) {
		Conditions = conditions;
	}
	/**
	 * @return the feeTotal
	 */
	public double getFeeTotal() {
		return FeeTotal;
	}
	/**
	 * @param feeTotal the feeTotal to set
	 */
	public void setFeeTotal(double feeTotal) {
		FeeTotal = feeTotal;
	}

}
