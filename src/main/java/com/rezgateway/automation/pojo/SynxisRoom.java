package com.rezgateway.automation.pojo;

import java.util.ArrayList;

public class SynxisRoom {

	private String roomTypeCode		= null;
	private String ratePlanCode		= null;
	private String totalAfterTax	= null;
	private ArrayList<SynxisCancellationPolicy> cp 	= new ArrayList<SynxisCancellationPolicy>();	
	
	public String getTotalAfterTax() {
		return totalAfterTax;
	}
	public void setTotalAfterTax(String totalAfterTax) {
		this.totalAfterTax = totalAfterTax;
	}
	public String getRoomTypeCode() {
		return roomTypeCode;
	}
	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}
	public String getRatePlanCode() {
		return ratePlanCode;
	}
	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}
	public ArrayList<SynxisCancellationPolicy> getCp() {
		return cp;
	}
	public void setCp(ArrayList<SynxisCancellationPolicy> cp) {
		this.cp = cp;
	}


}
